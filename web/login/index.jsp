<%--
  Created by IntelliJ IDEA.
  User: lucas.pereira
  Date: 11/06/2018
  Time: 10:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="imagens/mini-logo.gif">

    <title> Login | BOOK </title>

    <!-- Bootstrap core CSS -->
    <link href="/login/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/login/css/signin.css" rel="stylesheet">

    <link href="/login/css/floating-labels.css" rel="stylesheet">
</head>

<body class="text-center">
<form class="form-signin" action="logincontroller.do" method="post">
    <img class="mb-4" src="../imagens/books-logo-topo.gif" >
    <%--<h1 class="h3 mb-3 font-wei\ght-normal"> BOOK </h1>--%>
    <label for="txtemail" class="sr-only">Endereço de email</label>
    <input name="txtemail" id="txtemail" type="email" class="form-control" placeholder="Endereço de email" required
           autofocus>

    <label for="txtsenha" class="sr-only">Senha</label>
    <input type="password" id="txtsenha" class="form-control" placeholder="Senha" required>

    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> Lembre-me
        </label>
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
    <p class="mt-2 mb-3 text-muted">
        Ainda não tem usuário?
        <a href="../cadastro-usuario.jsp">Cadastre-se.</a>
    </p>
    <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
</form>
</body>
</html>
