<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Cadastro usu�rio | Books</title>
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.css"/>
    <script type="text/javascript" src="jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="bootstrap-3.3.5-dist/js/bootstrap.js"></script>

</head>
<body>

<div class="container">
    <div class="page-header">
        <h2 align="center">Cadastro Usu�rio | Books</h2>
    </div>
    <div class="row centered">
        <div class="col-sm-5">

            <form method="POST" action="cadastrousuario.do">

                <div class="form-group">
                    <label for="nome">Nome/Apelido</label>
                    <input type="text" class="form-control" name="txtnome">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="txtemail">
                </div>
                <div class="form-group">
                    <label for="senha">Senha</label>
                    <input type="password" class="form-control" name="txtsenha">
                </div>

                <div class="form-group">
                    <input class="btn btn-default" type="submit" value="Cadastrar">
                </div>

            </form>

        </div>
    </div><!--  row-->
</div>

</body>
</html>