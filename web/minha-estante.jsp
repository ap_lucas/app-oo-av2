<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%
    try {

        String user = (String) session.getAttribute("usuario");
        if (user.equals("")) {
            response.sendRedirect("login/index.jsp");
        }
    } catch (NullPointerException e) {
        response.sendRedirect("login/index.jsp");
    }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title> Minha estante | Books </title>

    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.css"/>
    <script type="text/javascript" src="jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="bootstrap-3.3.5-dist/js/bootstrap.js"></script>

</head>
<body>

<div class="container">
    <div class="row centered">
        <div id="logo" class="col-sm-6">
            <img src="imagens/books-logo-topo.gif">
        </div>
        <div id="menu" class="col-sm-4">
            <a href="home.jsp">home</a>
            <a href="minha-estante.jsp">minha estante</a>
            <a href="logoffcontroller.do">logoff</a>
        </div>
    </div>
</div>

</body>
</html>