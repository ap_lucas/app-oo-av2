<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>

<link rel="stylesheet" type="text/css" href="css/estilo.css">
<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.css" />
<script type="text/javascript" src="jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="bootstrap-3.3.5-dist/js/bootstrap.js"></script>

</head>
<body>
	
	<div class="container">
  		<div class="row centered">
  			<div id="logo" class="col-sm-6">
      			<img src="imagens/books-logo-topo.gif">
      			<!-- <h1>B<span class="azul">OO</span>K</h1> -->
    		</div>
    		<div id="menu" class="col-sm-4">
      			<a href="home.jsp">home</a> 
      			<a href="minha-estante.jsp">minha estante</a> 
      			<a href="login.jsp">login</a> 
    		</div>
  		</div>
  		
  		<div class="row">
  			<div class="col-sm-12">
  				<img src="imagens/book1.jpg" class="centered">
  			</div>
  		</div>	
	</div>
	
</body>
</html>