package dao;

import entidades.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UsuarioDAO {

    public void cadastrar(Usuario usu) {

        Connection con = Conexao.receberConexao();

        String sql = "INSERT INTO Usuario(nome,email,senha) VALUES (?,?,?)";

        try {
            PreparedStatement preparador = con.prepareStatement(sql);
            preparador.setString(1, usu.getNome());
            preparador.setString(2, usu.getEmail());
            preparador.setString(3, usu.getSenha());

            preparador.execute();

            preparador.close();

            System.out.println("Usu�rio cadastrado com sucesso!!!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void alterar(Usuario usu) {

        Connection con = Conexao.receberConexao();

        String sql = "UPDATE usuario SET nome=?, email=?, senha=? WHERE codigo=?";

        try {
            PreparedStatement preparador = con.prepareStatement(sql);
            preparador.setString(1, usu.getNome());
            preparador.setString(2, usu.getEmail());
            preparador.setString(3, usu.getSenha());
            preparador.setInt(4, usu.getCodigo());

            preparador.execute();

            preparador.close();

            System.out.println("Usu�rio alterado com sucesso!!!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void excluir(Usuario usu) {

        Connection con = Conexao.receberConexao();

        String sql = "DELETE FROM usuario WHERE codigo=?";

        try {
            PreparedStatement preparador = con.prepareStatement(sql);

            preparador.setInt(1, usu.getCodigo());

            preparador.execute();

            preparador.close();

            System.out.println("Usu�rio excluido com sucesso!!!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public boolean buscarLogin(String email, String senha) {

        boolean encontrou = false;

        Connection con = Conexao.receberConexao();

        String sql = "SELECT * FROM Usuario WHERE email = '?'";

        Usuario usuario = null;

        try {
            PreparedStatement preparador = con.prepareStatement(sql);
            preparador.setString(1, email);


            ResultSet resultado = preparador.executeQuery();

            usuario = new Usuario();

            while (resultado.next()) {
                usuario.setCodigo(resultado.getInt("codigo"));
                usuario.setNome(resultado.getString("nome"));
                usuario.setEmail(resultado.getString("email"));
                usuario.setSenha(resultado.getString("senha"));

                if (usuario.getEmail().equals(email) && usuario.getSenha().equals(senha)) {
                    encontrou = true;
//					break;
                }
            }

            con.close();

        } catch (SQLException e) {
            e.printStackTrace();

        }

        return encontrou;

    }

    public List<Usuario> buscarTodos() {

        Connection con = Conexao.receberConexao();

        String sql = "SELECT * FROM Usuario";

        ArrayList<Usuario> listaUsuario = new ArrayList<Usuario>();

        try {
            PreparedStatement preparador = con.prepareStatement(sql);

            ResultSet resultado = preparador.executeQuery();

            while (resultado.next()) {

                Usuario usuario = new Usuario();
                usuario.setCodigo(resultado.getInt("codigo"));
                usuario.setNome(resultado.getString("nome"));
                usuario.setEmail(resultado.getString("email"));
                usuario.setSenha(resultado.getString("senha"));

                listaUsuario.add(usuario);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return listaUsuario;

    }

}
