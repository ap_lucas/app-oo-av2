package controller;

import dao.UsuarioDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet implementation class LoginController
 */
//@WebServlet("/logincontroller.do")
@WebServlet(name = "Logar", urlPatterns = {"/logincontroller.do"})
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //doPost(request, response);
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
            String login = request.getParameter("txtemail");
            String senha = request.getParameter("txtsenha");
            boolean achou = false;

            HttpSession session = request.getSession();
            
            UsuarioDAO usuarioDao = new UsuarioDAO();
            
            achou = usuarioDao.buscarLogin(login, senha);

            if (achou = true) {
                response.sendRedirect("/minha-estante.jsp");
                session.setAttribute("usuario", login);
                session.setMaxInactiveInterval(60 * 5);
                System.out.println("Sucesso ao logar.");

            } else {
                response.sendRedirect("/login/index.jsp");
                session.setAttribute("usuario", "");
                System.out.println("Erro ao logar.");
            }
        } catch (NullPointerException e) {
            response.sendRedirect("/login/index.jsp");
        }
	}
		
	@Override
    public String getServletInfo() {
        return "Short description";
    }

    // DICA: https://www.javatpoint.com/servlet-http-session-login-and-logout-example	

}
