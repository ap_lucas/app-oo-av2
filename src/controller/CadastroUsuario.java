package controller;

import dao.UsuarioDAO;
import entidades.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class CadastroUsuario
 */
@WebServlet("/cadastrousuario.do")
public class CadastroUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Método POST
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// pega as informa��es da tela - view
		String nome = request.getParameter("txtnome"); // valor do atributo name do input
		String email = request.getParameter("txtemail");
		String senha = request.getParameter("txtsenha");

		System.out.println("nome: " + nome);
		System.out.println("email: " + email);
		System.out.println("senha: " + senha);

		// cria um objeto para jogar o que veio da view para o objeto (classe com os getters e setters)
		Usuario usuario = new Usuario(); // ctrl+shift+O para importar o pacote que est� a classe do Usuario
				
		usuario.setNome(nome);
		usuario.setEmail(email);
		usuario.setSenha(senha);


		// cria um objeto para mandar as informa��es do objeto usuario para o banco de dados
		UsuarioDAO usuarioDao = new UsuarioDAO();

		usuario.toString();

		usuarioDao.cadastrar(usuario);
				
		response.sendRedirect("/login/index.jsp");
	}

}
